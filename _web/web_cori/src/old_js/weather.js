const apiKey = "96e1a760418569c5d1087624b2b5d4c3";


let weather = {
    fetchDay: function(units){
        if (!units){units="imperial"}

        fetch("https://api.openweathermap.org/data/2.5/weather?lat=34.05&lon=-118.24"
        + "&units=" + units 
        + "&appid=" + apiKey
    )
    .then((response) => response.json())
    .then((data) => this.displayDay(data, units))
    },

    displayDay: function(data, units){
        if (units=="imperial") {tempUnits = "F"} else {units = "C"}

        //console.log(icon, description, temp, humidity, speed);
        document.querySelector(".rainy-1").innerText = data.weather[0].main;
        document.querySelector(".sat-19-mar-2022").innerText = moment().format('ddd, D MMM YYYY');
        document.querySelector(".weather-icon-Today").src = "img/rain-clouds@2x.png";
        data.main.temp
        document.querySelector(".x52-f").innerText = Math.floor(data.main.temp) + "° " + tempUnits;
    },

    fetchWeek: function(units){
        if (!units) {units="imperial"}

        fetch("https://api.openweathermap.org/data/2.5/forecast/daily?lat=34.05&lon=-118.24"
        + "&cnt=" + 7
        + "&units=" + units 
        + "&appid=" + apiKey
    )
    .then((response) => response.json())
    .then((data) => this.updateWeek(data, units))
    },

    updateWeek: function(data, units)
    {
        var currentDate = moment().format('ddd, D MMM');
        //if (units=="imperial") {tempUnits = "F"} else {units = "C"}
    
    
        var numberList = document.getElementById("weatherWeek");
        for(var i = 0; i < 7; i++)
        {
                let newListItem = document.createElement("div");
                newListItem.classList.add('weather-list');
                newListItem.id = i;
    
    
                let newListItemDay = document.createElement("div");
                newListItemDay.classList.add('weather-day');
                newListItemDay.id = i;
                newListItemDay.innerText = moment().add(i, 'days').format('ddd, D MMM');
                newListItem.appendChild(newListItemDay);
                
                let newListItemTemp = document.createElement("div");
                newListItemTemp.classList.add('weather-temp');
                newListItemTemp.id = i;
                newListItemTemp.innerText = "00"+"°";
                newListItem.appendChild(newListItemTemp);
    
    
                let newListItemIcon = document.createElement("img");
                newListItemIcon.classList.add('weather-icon');
                newListItemIcon.id = i;
                newListItemIcon.src = "img/rain-raining-svgrepo-com.svg";
                newListItem.appendChild(newListItemIcon);
                
                let newListItemMain = document.createElement("div");
                newListItemMain.classList.add('weather-main');
                newListItemMain.id = i;
                newListItemMain.innerText = "Rainy";
                newListItem.appendChild(newListItemMain);
    
                numberList.appendChild(newListItem);
        }
    }
}
