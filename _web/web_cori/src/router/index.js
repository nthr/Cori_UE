import { createRouter, createWebHistory } from 'vue-router'
import unknown_user from '../views/unknown_user.vue'
import user_menu from '../views/user_menu.vue'
import register from '../views/register.vue'
import nda from '../views/nda.vue'
import test from '../views/test.vue'

const routes = [
  {
    path: '/',
    name: 'unknown_user',
    component: unknown_user
  },
  {
    path: '/user_menu',
    name: 'user_menu',
    component: user_menu
  },
  {
    path: '/register',
    name: 'register',
    component: register
  },
  {
    path: '/nda',
    name: 'nda',
    component: nda
  },
  {
    path: '/test',
    name: 'test',
    component: test
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
