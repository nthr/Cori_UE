let weather = {
    "apiKey": "96e1a760418569c5d1087624b2b5d4c3",
    fetchWeather: function(units){
        if (!units){units="imperial"}

        fetch("https://api.openweathermap.org/data/2.5/weather?lat=34.05&lon=-118.24"
        + "&units=" + units 
        + "&appid=" + this.apiKey
    )
    .then((response) => response.json())
    .then((data) => this.displayWeather(data));
    },

    displayWeather: function(data, units){
        let tempUnits = "F";
        //if (units=="imperial") {tempUnits = "F"} else {units = "C"}

        const { icon, description } = data.weather[0];
        const { main, temp, humidity } = data.main;
        const { speed } = data.wind;
        console.log(icon, description, temp, humidity, speed);
        document.querySelector(".weather-main").innerText = main;
        document.querySelector(".weather-icon").src = "img/rain-clouds@2x.png";
        document.querySelector(".weather-description").innerText = description;
        document.querySelector(".weather-temp").innerText = temp + "° " + tempUnits;
        document.querySelector(".weather-humidity").innerText = "Humidity: " + humidity + "%";
        //document.querySelector(".wind").innerText = "Wind speed: " + speed + " " + units;
    }
}