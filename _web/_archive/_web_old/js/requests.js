function PUTButton(bAnim, index_Anim){
    // if bAnim is true, then function will call Animation Event in UE to play an animation and sound with selected or random id
    // rand if ID isn't set or not integer or bigger than available maxID
    let maxID = 12
    if(!index_Anim || index_Anim>maxID){
        index_Anim = Math.floor(Math.random() * (maxID - 0 + 1)) + 0;
    }

    if(bAnim){
        PUTRequest_anim('PUT', 'Button1', index_Anim)
    }
};


function PUTRequest_anim(method, functionName, index_Anim){
    let url = "http://127.0.0.1:30010/remote/object/call"
    let payload = {
      "objectPath" : "/Game/Maps/MHC_LightingPresets_RT/MHC_LightingPreset_Portrait_RT.MHC_LightingPreset_Portrait_RT:PersistentLevel.BP_CC4_Pawn_C_0",
      "functionName":functionName,
      "parameters": {
        "index_Anim":index_Anim
      },
      "generateTransaction":true
    }

    let options = {
        method: method,
        headers:{'Content-Type':'application/json'},
        body: JSON.stringify(payload)
    }

    fetch(url, options)
    .then(response => console.log(response.status))
}