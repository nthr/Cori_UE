window.onload = function()
{
    document.querySelector(".schedule-day").innerText = moment().format('MMM Do');
    
    weather.fetchDay();
    weather.updateWeek();

    time.updateTime();
}

let time = {
    updateTime: function(){
        let currentDate = new Date();
        var h =  currentDate.getHours(), m = currentDate.getMinutes();
        var time = (h > 12) ? (h-12 + ':' + m +' PM') : (h + ':' + m +' AM');

        document.querySelector(".x1032-am").innerText = time;
        setInterval("time.updateTime()", 10000);

        if (m == "00") weather.fetchDay(); //update weather every hour
    }
}

